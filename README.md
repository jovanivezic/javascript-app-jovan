### JavaScript edukacija ###

* Lession 01:
    
    Task 1:
        - Kreirati jedan zeleni kvadrat sa leve strane i kontejner sa desne strane. 
        - Klik na zeleni kvadrat kreira potpuno isti kvadrat samo crvene boje. 
        - Ukoliko levi kvadrat ima ID pri kloniranju treba ga ukloniti.
    
    Task 2
        - Dodati brojac 
        - Dodati opciju da kada se klikne na crveni kvadratic da se ukloni i istovremeno treba da azurira stanje countera
        - Sacuvati trenutno stanje (counter i broj kockica u local storage). 

    Task 3
        - napraviti objektno orjentisani kod na jedan od 3 nacina definisanja objekata (literal, first class function or class)

    Task 4
        1. nakon refresh-a da se prikaze zatamljeni ekran sa input poljem za unos imena
        2. ime treba da se veze sa countom
        3. kada se unese novo ime zapise se u localstorage kao novi record
        4. kada se unese isto ime, prikazati stanje iz localstorage-a
        5. ucitati sliku sa nekog public api-a i prikazati u okviru crvenog box-a na svaki click
    Task 5 
        1. Posto je objekat postao prevelik, rasclaniti ga na vise klasa ili objekata koji ce biti povezani. 
