'use strict';


var CloningApp = function() {

    this.getLocalStorage();
    this.cloneThisElement.addEventListener('click', this.cloneElement.bind(this));
    document.addEventListener('click', this.removeClone.bind(this));

}

CloningApp.prototype = {
    cloneThisElement: document.getElementById('js_clone_original'),
    counter: 0,
    counterOutput: document.getElementById('js_counter'),
    clonesHolder: document.getElementById('js_clones'),

    cloneElement: function() {
        var clone = this.cloneThisElement.cloneNode(true);
        clone.removeAttribute('id');
        clone.classList.add('cloned', 'js_remove_clone');
        this.clonesHolder.appendChild(clone);
        this.counter ++;
        this.counterOutput.innerHTML = this.counter;
        localStorage.setItem('numberOfSquares', this.counter);
    },

    removeClone: function() {
        if (event.target.classList.contains('js_remove_clone')) {
            var clickedEl = event.target;
            clickedEl.remove();
            this.counter --;
            this.counterOutput.innerHTML = this.counter;
            localStorage.setItem('numberOfSquares', this.counter);
        }
    },

    getLocalStorage: function() {
        var localStorageCount = localStorage.getItem('numberOfSquares');
        var localStorageCountInt = parseInt(localStorageCount);

        if (localStorageCount !== null) {
            this.counterOutput.innerHTML = localStorageCount;

            for(let i = 0; i < localStorageCountInt; i++) {
                this.cloneElement();
            }
        }
    }

}

new CloningApp();