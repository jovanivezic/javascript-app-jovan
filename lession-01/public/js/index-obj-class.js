'use strict';

class CloningApp {
    cloneThisElement = document.getElementById('js_clone_original');
    counter = 0;
    counterOutput = document.getElementById('js_counter');
    clonesHolder = document.getElementById('js_clones');

    constructor() {
        this.openModal();
        this.checkUsername();
        this.cloneThisElement.addEventListener('click', this.cloneElement.bind(this));
        document.addEventListener('click', this.removeClone.bind(this));
    }

    cloneElement() {
        var clone = this.cloneThisElement.cloneNode(true);
        clone.removeAttribute('id');
        clone.classList.add('cloned', 'js_remove_clone');
        this.clonesHolder.appendChild(clone);
        this.counter ++;
        this.counterOutput.innerHTML = this.counter;
        this.updateLocalStorage();
    }

    removeClone() {
        if (event.target.classList.contains('js_remove_clone')) {
            var clickedEl = event.target;
            clickedEl.remove();
            this.counter --;
            this.counterOutput.innerHTML = this.counter;
            this.updateLocalStorage();
        }
    }

    //set local storage for every new username
    setLocalStorage() {
        var name = document.getElementById('js_username').innerHTML;
        var count = document.getElementById('js_counter').innerHTML;
        
        if(localStorage.length) {
            var usersArray = JSON.parse(localStorage.users);
        } else {
            var usersArray = [];
        }
        usersArray.push({
            name: name,
            count: count
        });
        
        localStorage.setItem('users', JSON.stringify(usersArray));
    }

    //update local storage after cloning for some user
    updateLocalStorage() {
        var usersArray = JSON.parse(localStorage.users);
        var name = document.getElementById('js_username').innerHTML;
        var count = document.getElementById('js_counter').innerHTML;

        usersArray.forEach(function(item) {
            if(item.name === name) {
                item.count = count;
            }
        });

        localStorage.setItem('users', JSON.stringify(usersArray));
    }

    openModal() {
        document.getElementById('js_modal').classList.add('is_active');
    }

    closeModal() {
        document.getElementById('js_modal').classList.remove('is_active');
    }

    getLocalStorage(name, count) {
        var username = document.getElementById('js_username');
        username.innerHTML = name;
        this.counterOutput.innerHTML = count;

        for(let i = 0; i < count; i++) {
            this.cloneElement();
        }

        this.closeModal();
    }

    submitUsername() {
        var formEl = document.getElementById('js_form');
        var username = document.getElementById('js_username');
        var inputValue = document.getElementById('js_input_username').value;

        if (inputValue !== '') {
            this.closeModal();
            username.innerHTML = inputValue;
            this.setLocalStorage();
        } else {
            var error = document.createElement('div');
            error.innerHTML = 'You must insert username';
            error.classList.add('error');
            formEl.insertBefore(error, formEl.childNodes[0]);
        }
    }

    checkUsername() {
        var formEl = document.getElementById('js_form');
        
        formEl.addEventListener('submit', () => {
            if (localStorage.length) {
                var inputValue = document.getElementById('js_input_username').value;
                var usersArray = JSON.parse(localStorage.users);
                var exists = false;

                usersArray.forEach((item) => {
                    if (item.name === inputValue) {
                        console.log("Korisnicko ime vec postoji", item);
                        exists = true;
                        var name = item.name;
                        var count = item.count;
                        this.getLocalStorage(name, count);
                    }
                });

                if (!exists) {
                    this.submitUsername();
                }

            } else {
                this.submitUsername();
            }

        }); 
    }
}

new CloningApp();