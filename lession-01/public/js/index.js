'use strict';

const cloneThisElement = document.getElementById('js_clone_original');
var counter = 0;
const counterOutput = document.getElementById('js_counter');
const clonesHolder = document.getElementById('js_clones');

function cloneElement() {
    var clone = cloneThisElement.cloneNode(true);
    clone.removeAttribute('id');
    clone.classList.add('cloned', 'js_remove_clone');
    clonesHolder.appendChild(clone);
    counter ++;
    counterOutput.innerHTML = counter;
    localStorage.setItem('numberOfSquares', counter);
};

cloneThisElement.addEventListener('click', cloneElement);

(function removeClone() {
    document.querySelector('body').addEventListener('click', function(event) {
        if (event.target.classList.contains('js_remove_clone')) {
            var clickedEl = event.target;
            clickedEl.remove();
            counter --;
            counterOutput.innerHTML = counter;
            localStorage.setItem('numberOfSquares', counter);
        }
    });
})();

(function getLocalStorage() {
    var localStorageCount = localStorage.getItem('numberOfSquares');
    var localStorageCountInt = parseInt(localStorageCount);

    if (localStorageCount !== null) {
        counterOutput.innerHTML = localStorageCount;

        for(let i = 0; i < localStorageCountInt; i++) {
            cloneElement();
        }
    }
})();